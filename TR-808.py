# TR-808 Drum Machine

import pygame
from pygame import mixer

pygame.init()

WIDTH = 1600
HEIGHT = 1000

black = (0, 0, 0)
white = (255, 255, 255)
gray = (128, 128, 128)
light_gray = (170, 170, 170)
dark_gray = (50, 50, 50)
green = (0, 255, 0)
gold = (212, 175, 55)
blue = (0, 255, 255)

roland_black = (44, 43, 39)
roland_label_gray = (169, 163, 147)
roland_label_yellow = (255,255,159)
roland_label_red = (252, 76, 24)
#roland_label_red = (252, 53, 0)
roland_button_red = (224, 50, 39)
roland_button_orange = (255, 120, 36)
roland_button_yellow = (253, 233, 0)
roland_button_white =(255, 254, 215)

screen = pygame.display.set_mode([WIDTH, HEIGHT])
pygame.display.set_caption('TR-808.Me')
label_font = pygame.font.Font('RolandTR505.ttf', 32)
medium_font = pygame.font.Font('RolandTR505.ttf', 24)

font_large = pygame.font.Font('RolandTR505.ttf', 20)
track_font = pygame.font.Font('RolandTR505.ttf', 18)
controls_font = pygame.font.Font('RolandTR505.ttf', 18)
small_font = pygame.font.Font('RolandTR505.ttf', 16)

index = 100
fps = 60
timer = pygame.time.Clock()
beats = 16
instruments = 27
boxes = []
clicked = [[-1 for _ in range(beats)] for _ in range(instruments)]
active_list = [1 for _ in range(instruments)]
bpm = 120
playing = True
active_length = 0
active_beat = 0
beat_changed = True
save_menu = False
load_menu = False
projects = []
file = open('projects.txt', 'r')
for line in file:
    projects.append(line)
beat_name = ''
typing = False


# load in sounds
kick = mixer.Sound('TR-808 kit/Kick.wav')
snare1 = mixer.Sound('TR-808 kit/Snare 1.wav')
snare2 = mixer.Sound('TR-808 kit/Snare 2.wav')
clap1 = mixer.Sound('TR-808 kit/Clap 1.wav')
clap2 = mixer.Sound('TR-808 kit/Clap 2.wav')
rim_shot = mixer.Sound('TR-808 kit/Rim Shot.wav')
hi_hat1 = mixer.Sound('TR-808 kit/Closed Hat 1.wav')
hi_hat2 = mixer.Sound('TR-808 kit/Closed Hat 2.wav')
hi_hat3 = mixer.Sound('TR-808 kit/Closed Hat 3.wav')
hi_hat4 = mixer.Sound('TR-808 kit/Open Hat 1.wav')
hi_hat5 = mixer.Sound('TR-808 kit/Open Hat 2.wav')
hi_hat6 = mixer.Sound('TR-808 kit/Open Hat 3.wav')
maraca1 = mixer.Sound('TR-808 kit/Maraca 1.wav')
maraca2 = mixer.Sound('TR-808 kit/Maraca 2.wav')
clave = mixer.Sound('TR-808 kit/Clave.wav')
cowbell = mixer.Sound('TR-808 kit/Cow Bell.wav')
tom_hi = mixer.Sound('TR-808 kit/Tom Hi.wav')
tom_mid = mixer.Sound('TR-808 kit/Tom Mid.wav')
tom_low = mixer.Sound('TR-808 kit/Tom Low.wav')
conga_hi = mixer.Sound('TR-808 kit/Conga Hi.wav')
conga_mid = mixer.Sound('TR-808 kit/Conga Mid.wav')
conga_low = mixer.Sound('TR-808 kit/Conga Low.wav')
perc1 = mixer.Sound('TR-808 kit/Perc 1.wav')
perc2 = mixer.Sound('TR-808 kit/Perc 2.wav')
perc3 = mixer.Sound('TR-808 kit/Perc 3.wav')
cymbal = mixer.Sound('TR-808 kit/Cymbal.wav')
sub = mixer.Sound('TR-808 kit/808 Sub.wav')
pygame.mixer.set_num_channels(instruments * 3)


def play_notes():
    for ins in range(len(clicked)):
        if clicked[ins][active_beat] == 1 and active_list[ins] == 1:
            if ins == 0:
                kick.play()
            if ins == 1:
                snare1.play()
            if ins == 2:
                snare2.play()
            if ins == 3:
                clap1.play()
            if ins == 4:
                clap2.play()
            if ins == 5:
                rim_shot.play()
            if ins == 6:
                hi_hat1.play()
            if ins == 7:
                hi_hat2.play()
            if ins == 8:
                hi_hat3.play()
            if ins == 9:
                hi_hat4.play()
            if ins == 10:
                hi_hat5.play()
            if ins == 11:
                hi_hat6.play()
            if ins == 12:
                maraca1.play()
            if ins == 13:
                maraca2.play()
            if ins == 14:
                clave.play()
            if ins == 15:
                cowbell.play()
            if ins == 16:
                tom_hi.play()
            if ins == 17:
                tom_mid.play()
            if ins == 18:
                tom_low.play()
            if ins == 19:
                conga_hi.play()
            if ins == 20:
                conga_mid.play()
            if ins == 21:
                conga_low.play()
            if ins == 22:
                perc1.play()
            if ins == 23:
                perc2.play()
            if ins == 24:
                perc3.play()
            if ins == 25:
                cymbal.play()
            if ins == 26:
                sub.play()


# draw.rect(put on screen, color, [Starting Width, Starting Height, Width, Height,], line thickness, rounded)
def draw_grid(clicks, beat, actives):
    left_box = pygame.draw.rect(screen, roland_label_gray, [0, 0, 203, HEIGHT - 188], 3)
    left_background = pygame.draw.rect(screen, roland_label_yellow, [3, 3, 197, HEIGHT - 193], 0, 0)
    bottom_box = pygame.draw.rect(screen, roland_label_gray, [0, HEIGHT - 190, WIDTH, 190], 3)
    boxes = []
    colors = [roland_label_gray, roland_black, roland_label_gray]
    kick_text = track_font.render('Kick', True, colors[actives[0]])
    screen.blit(kick_text, (10, 11))
    snare1_text = track_font.render('Snare 1', True, colors[actives[1]])
    screen.blit(snare1_text, (10, 41))
    snare2_text = track_font.render('Snare 2', True, colors[actives[2]])
    screen.blit(snare2_text, (10, 71))
    clap1_text = track_font.render('Clap 1', True, colors[actives[3]])
    screen.blit(clap1_text, (10, 101))
    clap2_text = track_font.render('Clap 2', True, colors[actives[4]])
    screen.blit(clap2_text, (10, 131))
    rim_shot_text = track_font.render('Rim Shot', True, colors[actives[5]])
    screen.blit(rim_shot_text, (10, 161))
    hi_hat1_text = track_font.render('Closed Hat 1', True, colors[actives[6]])
    screen.blit(hi_hat1_text, (10, 191))
    hi_hat2_text = track_font.render('Closed Hat 2', True, colors[actives[7]])
    screen.blit(hi_hat2_text, (10, 221))
    hi_hat3_text = track_font.render('Closed Hat 3', True, colors[actives[8]])
    screen.blit(hi_hat3_text, (10, 251))
    hi_hat4_text = track_font.render('Open Hat 1', True, colors[actives[9]])
    screen.blit(hi_hat4_text, (10, 281))
    hi_hat5_text = track_font.render('Open Hat 2', True, colors[actives[10]])
    screen.blit(hi_hat5_text, (10, 311))
    hi_hat6_text = track_font.render('Open Hat 3', True, colors[actives[11]])
    screen.blit(hi_hat6_text, (10, 341))
    maraca1_text = track_font.render('Maraca 1', True, colors[actives[12]])
    screen.blit(maraca1_text, (10, 371))
    maraca2_text = track_font.render('Maraca 2', True, colors[actives[13]])
    screen.blit(maraca2_text, (10, 401))
    clave_text = track_font.render('Clave', True, colors[actives[14]])
    screen.blit(clave_text, (10, 431))
    cowbell_text = track_font.render('Cow Bell', True, colors[actives[15]])
    screen.blit(cowbell_text, (10, 461))
    tom_hi_text = track_font.render('Tom Hi', True, colors[actives[16]])
    screen.blit(tom_hi_text, (10, 491))
    tom_mid_text = track_font.render('Tom Mid', True, colors[actives[17]])
    screen.blit(tom_mid_text, (10, 521))
    tom_low_text = track_font.render('Tom Low', True, colors[actives[18]])
    screen.blit(tom_low_text, (10, 551))
    conga_hi_text = track_font.render('Conga Hi', True, colors[actives[19]])
    screen.blit(conga_hi_text, (10, 581))
    conga_mid_text = track_font.render('Conga Mid', True, colors[actives[20]])
    screen.blit(conga_mid_text, (10, 611))
    conga_low_text = track_font.render('Conga Low', True, colors[actives[21]])
    screen.blit(conga_low_text, (10, 641))
    perc1_text = track_font.render('Percussion 1', True, colors[actives[22]])
    screen.blit(perc1_text, (10, 671))
    perc2_text = track_font.render('Percussion 2', True, colors[actives[23]])
    screen.blit(perc2_text, (10, 701))
    perc3_text = track_font.render('Percussion 3', True, colors[actives[24]])
    screen.blit(perc3_text, (10, 731))
    cymbal_text = track_font.render('Cymbal', True, colors[actives[25]])
    screen.blit(cymbal_text, (10, 761))
    sub808_text = track_font.render('808 Sub', True, colors[actives[26]])
    screen.blit(sub808_text, (10, 791))
    for i in range(instruments):
        pygame.draw.line(screen, roland_label_gray, (0, (i * 30) + 30), (200, (i * 30) + 30), 3)

    for i in range(beats):
        for j in range(instruments):
            if clicks[j][i] == -1:
                color = roland_button_white
            else:
                if actives[j] == 1:
                    color = roland_button_orange
                else:
                    color = roland_black
            rectangle = pygame.draw.rect(screen, color,
                             # (screen, color, [X start position, Y start position, actual width, actual height], border thickness, rounded)
                             [i * ((WIDTH - 200) // beats) + 203, (j * 30) + 1,
                              ((WIDTH - 200) // beats) - 3, ((HEIGHT - 30) // instruments) - 8], 0, 1)
            pygame.draw.rect(screen, gold,
                             [i * ((WIDTH - 200) // beats) + 202, (j * 30) + 1,
                              ((WIDTH - 200) // beats), ((HEIGHT - 30) // instruments) - 6], 5, 5)
            pygame.draw.rect(screen, black,
                             [i * ((WIDTH - 200) // beats) + 202, (j * 30),
                              ((WIDTH - 200) // beats), ((HEIGHT - 30) // instruments)], 2, 5)
            boxes.append((rectangle, (i, j)))

    active = pygame.draw.rect(screen, roland_button_orange, [beat * ((WIDTH - 200) // beats) + 200, 0,
                                                                ((WIDTH - 200) // beats), instruments * 30], 3, 3)
    return boxes


def draw_save_menu(beat_name, typing):
    pygame.draw.rect(screen, black, [0, 0, WIDTH, HEIGHT])
    menu_text = label_font.render('SAVE MENU: Enter a Name for this beat', True, white)
    screen.blit(menu_text, (400, 40))
    saving_btn = pygame.draw.rect(screen, gray, [WIDTH // 2 - 150, HEIGHT * 0.75, 150, 100], 0, 5)
    saving_text = label_font.render('Save', True, white)
    screen.blit(saving_text, (WIDTH // 2 - 110, HEIGHT * 0.75 + 30))
    exit_btn = pygame.draw.rect(screen, gray, [WIDTH - 200, HEIGHT - 100, 170, 90], 0, 5)
    exit_text = label_font.render('Close', True, white)
    screen.blit(exit_text, [WIDTH - 160, HEIGHT - 70])
    if typing:
        pygame.draw.rect(screen, dark_gray, [400, 200, 600, 200], 0, 5)
    entry_rect = pygame.draw.rect(screen, gray, [400, 200, 600, 200], 5, 5)
    entry_text = label_font.render(f'{beat_name}', True, white)
    screen.blit(entry_text, (430, 250))
    return exit_btn, saving_btn, entry_rect


def draw_load_menu(index):
    loaded_clicked = []
    loaded_beats = 0
    loaded_bpm = 0
    pygame.draw.rect(screen, black, [0, 0, WIDTH, HEIGHT])
    menu_text = label_font.render('LOAD MENU: Select Saved Beat', True, white)
    screen.blit(menu_text, (400, 40))
    loading_btn = pygame.draw.rect(screen, gray, [WIDTH // 2 - 150, HEIGHT * 0.87, 150, 100], 0, 5)
    loading_text = label_font.render('Load', True, white)
    screen.blit(loading_text, (WIDTH // 2 - 110, HEIGHT * 0.87 + 30))
    delete_btn = pygame.draw.rect(screen, gray, [(WIDTH // 2) - 500, HEIGHT * 0.87, 150, 100], 0, 5)
    delete_text = label_font.render('Delete', True, white)
    screen.blit(delete_text, ((WIDTH // 2) - 475, HEIGHT * 0.87 + 30))
    exit_btn = pygame.draw.rect(screen, gray, [WIDTH - 200, HEIGHT - 100, 170, 90], 0, 5)
    exit_text = label_font.render('Close', True, white)
    screen.blit(exit_text, [WIDTH - 160, HEIGHT - 70])
    loaded_rectangle = pygame.draw.rect(screen, gray, [190, 90, 1000, 600], 5, 5)
    if 0 <= index < len(projects):
        pygame.draw.rect(screen, light_gray, [190, 100 + index * 50, 1000, 50])
    for beat in range(len(projects)):
        if beat < 10:
            beat_clicked = []
            row_text = medium_font.render(f'{beat + 1}', True, white)
            screen.blit(row_text, (200, 100 + beat * 50))
            name_index_start = projects[beat].index('name: ') + 6
            name_index_end = projects[beat].index(', beats:')
            name_text = medium_font.render(projects[beat][name_index_start:name_index_end], True, white)
            screen.blit(name_text, (240, 100 + beat * 50))
        if 0 <= index < len(projects) and beat == index:
            beats_index_end = projects[beat].index(', bpm:')
            loaded_beats = int(projects[beat][name_index_end + 8:beats_index_end])
            bpm_index_end = projects[beat].index(', selected:')
            loaded_bpm = int(projects[beat][beats_index_end + 6:bpm_index_end])
            loaded_clicks_string = projects[beat][bpm_index_end + 14: -3]
            loaded_clicks_rows = list(loaded_clicks_string.split("], ["))
            for row in range(len(loaded_clicks_rows)):
                loaded_clicks_row = (loaded_clicks_rows[row].split(', '))
                for item in range(len(loaded_clicks_row)):
                    if loaded_clicks_row[item] == '1' or loaded_clicks_row[item] == '-1':
                        loaded_clicks_row[item] = int(loaded_clicks_row[item])
                beat_clicked.append(loaded_clicks_row)
                loaded_clicked = beat_clicked
    loaded_info = [loaded_beats, loaded_bpm, loaded_clicked]
    return exit_btn, loading_btn, delete_btn, loaded_rectangle, loaded_info


run = True
while run:
    timer.tick(fps)
    screen.fill(roland_black)
    boxes = draw_grid(clicked, active_beat, active_list)
    #lower menu buttons
    play_pause = pygame.draw.rect(screen, roland_button_red, [50, HEIGHT - 140, 100, 100], 0, 5)
    if playing:
        play_text2 = medium_font.render('Play', True, roland_button_white)
        screen.blit(play_text2, (68, HEIGHT - 105))
    else:
        play_text2 = medium_font.render('Pause', True, roland_label_gray)
        screen.blit(play_text2, (63, HEIGHT - 105))
    # bpm stuff
    bpm_rect = pygame.draw.rect(screen, roland_button_red, [175, HEIGHT - 140, 100, 100], 0, 5)
    bpm_text = medium_font.render('BPM', True, roland_button_white)
    screen.blit(bpm_text, (196, HEIGHT - 125))
    bpm_text2 = medium_font.render(f'{bpm}', True, roland_button_white)
    screen.blit(bpm_text2, (201, HEIGHT - 85))
    bpm_add_rect = pygame.draw.rect(screen, roland_button_orange, [365, HEIGHT - 140, 48, 48], 0, 5)
    bpm_sub_rect = pygame.draw.rect(screen, roland_button_orange, [365, HEIGHT - 90, 48, 48], 0, 5)
    bpm_add1_rect = pygame.draw.rect(screen, roland_button_orange, [295, HEIGHT - 140, 48, 48], 0, 5)
    bpm_sub1_rect = pygame.draw.rect(screen, roland_button_orange, [295, HEIGHT - 90, 48, 48], 0, 5)
    add_text = medium_font.render('+5', True, roland_button_white)
    sub_text = medium_font.render('-5', True, roland_button_white)
    add1_text = medium_font.render('+1', True, roland_button_white)
    sub1_text = medium_font.render('-1', True, roland_button_white)
    screen.blit(add_text, (375, HEIGHT - 125))
    screen.blit(sub_text, (375, HEIGHT - 75))
    screen.blit(add1_text, (305, HEIGHT - 125))
    screen.blit(sub1_text, (305, HEIGHT - 75))
    # beats stuff
    beats_rect = pygame.draw.rect(screen, roland_button_white, [435, HEIGHT - 140, 100, 100], 0, 5)
    beats_text = medium_font.render('Time', True, roland_label_gray)
    screen.blit(beats_text, (455, HEIGHT - 125))
    beats_text2 = label_font.render(f'{beats}', True, roland_label_gray)
    screen.blit(beats_text2, (470, HEIGHT - 90))
    beats_add_rect = pygame.draw.rect(screen, roland_button_yellow, [555, HEIGHT - 140, 48, 48], 0, 5)
    beats_sub_rect = pygame.draw.rect(screen, roland_button_yellow, [555, HEIGHT - 90, 48, 48], 0, 5)
    add_text2 = medium_font.render('+1', True, roland_button_white)
    sub_text2 = medium_font.render('-1', True, roland_button_white)
    screen.blit(add_text2, (565, HEIGHT - 130))
    screen.blit(sub_text2, (565, HEIGHT - 80))
    # instruments rects
    instrument_rects = []
    for i in range(instruments):
        rect = pygame.rect.Rect((0, i * 30), (200, 30))
        instrument_rects.append(rect)
    # save and load stuff
    save_button = pygame.draw.rect(screen, roland_button_yellow, [625, HEIGHT - 140, 100, 48], 0, 5)
    load_button = pygame.draw.rect(screen, roland_button_yellow, [625, HEIGHT - 90, 100, 48], 0, 5)
    save_text = medium_font.render('Save', True, roland_button_white)
    load_text = medium_font.render('Load', True, roland_button_white)
    screen.blit(save_text, (640, HEIGHT - 133))
    screen.blit(load_text, (640, HEIGHT - 83))
    # clear board
    clear_button = pygame.draw.rect(screen, roland_label_gray, [745, HEIGHT - 120, 100, 48], 0, 5)
    clear_text = medium_font.render('Clear', True, roland_button_white)
    screen.blit(clear_text, (755, HEIGHT - 113))
    #logo
    roland_logo = pygame.image.load('images/Roland Logo.png')
    roland_logo = pygame.transform.scale(roland_logo, (240, 48.5))
    screen.blit(roland_logo, (WIDTH - 750, HEIGHT - 175))

    roland_logo2 = medium_font.render('Rhythm Composer', True, roland_label_red)
    roland_logo3 = medium_font.render('TR-808', True, roland_label_red)
    roland_logo4 = small_font.render('Computer Controlled', True, roland_label_gray)
    screen.blit(roland_logo2, (WIDTH - 500, HEIGHT - 160))
    screen.blit(roland_logo3, (WIDTH - 250, HEIGHT - 160))
    screen.blit(roland_logo4, (WIDTH - 250, HEIGHT - 130))

    logo_line = pygame.draw.line(screen, roland_label_red, (WIDTH - 750, HEIGHT - 130), (WIDTH, HEIGHT - 130), 3)
    logo_line2 = pygame.draw.line(screen, roland_button_orange, (WIDTH - 150, HEIGHT - 138), (WIDTH, HEIGHT - 138), 3)
    logo_line3 = pygame.draw.line(screen, roland_button_yellow, (WIDTH - 115, HEIGHT - 146), (WIDTH, HEIGHT - 146), 3)
    logo_line4 = pygame.draw.line(screen, roland_button_white, (WIDTH - 75, HEIGHT - 154), (WIDTH, HEIGHT - 154), 3)
    if save_menu:
        exit_button, saving_button, entry_rectangle = draw_save_menu(beat_name, typing)
    if load_menu:
        exit_button, loading_button, delete_button, loaded_rectangle, loaded_info = draw_load_menu(index)
    if beat_changed:
        play_notes()
        beat_changed = False
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN and not save_menu and not load_menu:
            for i in range(len(boxes)):
                print(boxes)
                if boxes[i][0].collidepoint(event.pos):
                    coords = boxes[i][1]
                    clicked[coords[1]][coords[0]] *= -1
        if event.type == pygame.MOUSEBUTTONUP and not save_menu and not load_menu:
            if play_pause.collidepoint(event.pos):
                if playing:
                    playing = False
                elif not playing:
                    playing = True
            elif bpm_add_rect.collidepoint(event.pos):
                bpm += 5
            elif bpm_sub_rect.collidepoint(event.pos):
                bpm -= 5
            elif bpm_add1_rect.collidepoint(event.pos):
                bpm += 1
            elif bpm_sub1_rect.collidepoint(event.pos):
                bpm -= 1
            elif beats_add_rect.collidepoint(event.pos):
                beats += 1
                for i in range(len(clicked)):
                    clicked[i].append(-1)
            elif beats_sub_rect.collidepoint(event.pos):
                beats -= 1
                for i in range(len(clicked)):
                    clicked[i].pop(-1)
            elif clear_button.collidepoint(event.pos):
                clicked = [[-1 for _ in range(beats)] for _ in range(instruments)]
            elif save_button.collidepoint(event.pos):
                save_menu = True
                playing = False
            elif load_button.collidepoint(event.pos):
                load_menu = True
                playing = False
            for i in range(len(instrument_rects)):
                if instrument_rects[i].collidepoint(event.pos):
                    active_list[i] *= -1
        elif event.type == pygame.MOUSEBUTTONUP:
            if exit_button.collidepoint(event.pos):
                save_menu = False
                load_menu = False
                playing = True
                beat_name = ''
                typing = False
            if load_menu:
                if loaded_rectangle.collidepoint(event.pos):
                    index = (event.pos[1] - 100) // 50
                if delete_button.collidepoint(event.pos):
                    if 0 <= index < len(projects):
                        projects.pop(index)
                if loading_button.collidepoint(event.pos):
                    if 0 <= index < len(projects):
                        beats = loaded_info[0]
                        bpm = loaded_info[1]
                        clicked = loaded_info[2]
                        index = 100
                        save_menu = False
                        load_menu = False
                        playing = True
                        typing = False
            if save_menu:
                if entry_rectangle.collidepoint(event.pos):
                    if typing:
                        typing = False
                    elif not typing:
                        typing = True
                if saving_button.collidepoint(event.pos):
                    file = open('projects.txt', 'w')
                    projects.append(f'\nname: {beat_name}, beats: {beats}, bpm: {bpm}, selected: {clicked}')
                    for i in range(len(projects)):
                        file.write(str(projects[i]))
                    file.close()
                    save_menu = False
                    load_menu = False
                    playing = True
                    typing = False
                    beat_name = ''
        if event.type == pygame.TEXTINPUT and typing:
            beat_name += event.text
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSPACE and len(beat_name) > 0 and typing:
                beat_name = beat_name[:-1]

    beat_length = 900 // bpm

    if playing:
        if active_length < beat_length:
            active_length += 1
        else:
            active_length = 0
            if active_beat < beats - 1:
                active_beat += 1
                beat_changed = True
            else:
                active_beat = 0
                beat_changed = True

    pygame.display.flip()

file = open('projects.txt', 'w')
for i in range(len(projects)):
    file.write(str(projects[i]))
file.close()
pygame.quit()
